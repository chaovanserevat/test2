<?php

$connection = new mysqli("localhost", "root", "", "document");

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$connection->query("SET CHARACTER = 'utf8'");
$connection->query("SET SESSION collation_connection = 'utf8_general_ci'");

include_once ("get-project-path.php");

$_base_url = $_SESSION["base_url"] = base_url();

$_report_url = "../../../";

?>