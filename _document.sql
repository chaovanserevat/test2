/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50045
Source Host           : localhost:3306
Source Database       : document

Target Server Type    : MYSQL
Target Server Version : 50045
File Encoding         : 65001

Date: 2012-09-16 19:14:34
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `tbl_activity`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_activity`;
CREATE TABLE `tbl_activity` (
  `id` int(11) NOT NULL auto_increment,
  `user` int(11) default NULL,
  `action` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `description` varchar(500) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_activity
-- ----------------------------
INSERT INTO tbl_activity VALUES ('1', '1', 'login', '0000-00-00 00:00:00', 'success');
INSERT INTO tbl_activity VALUES ('2', '2', 'login', '0000-00-00 00:00:00', 'failed 1st');
INSERT INTO tbl_activity VALUES ('3', '2', 'login', '0000-00-00 00:00:00', 'failed 2nd');
INSERT INTO tbl_activity VALUES ('4', '2', 'login', '0000-00-00 00:00:00', 'failed 3rd');
INSERT INTO tbl_activity VALUES ('5', '2', 'login', '0000-00-00 00:00:00', 'failed added to blacklist');
INSERT INTO tbl_activity VALUES ('6', '3', 'login', '0000-00-00 00:00:00', 'success');
INSERT INTO tbl_activity VALUES ('7', '3', 'create', '0000-00-00 00:00:00', 'receive new document');
INSERT INTO tbl_activity VALUES ('8', '3', 'delete', '0000-00-00 00:00:00', 'deleted document at 10am on 3-3-2012');
INSERT INTO tbl_activity VALUES ('9', '3', 'update', '0000-00-00 00:00:00', 'update profile');
INSERT INTO tbl_activity VALUES ('12', '0', 'login', '2012-04-04 04:04:38', 'sucessfully login');
INSERT INTO tbl_activity VALUES ('13', '0', 'login', '2012-04-04 04:04:59', 'user: hacker (login failed 3)');
INSERT INTO tbl_activity VALUES ('14', '11', 'login', '2012-04-04 04:04:46', 'sucessfully login');
INSERT INTO tbl_activity VALUES ('15', '0', 'login', '2012-04-04 04:04:06', 'user: hacker and password: 256c6afc9322c59d1849411040030990 (login failed 3)');
INSERT INTO tbl_activity VALUES ('16', '0', 'login', '2012-04-04 04:04:03', 'user: hacker and password: hack this system (login failed 2)');
INSERT INTO tbl_activity VALUES ('17', '0', 'login', '2012-04-04 04:04:29', 'user: hacker and password: hack this system (login failed 1)');
INSERT INTO tbl_activity VALUES ('18', '11', 'login', '2012-04-05 01:04:58', 'sucessfully login');
INSERT INTO tbl_activity VALUES ('19', '11', 'login', '2012-04-05 01:04:00', 'sucessfully login');
INSERT INTO tbl_activity VALUES ('20', '11', 'login', '2012-04-20 03:04:22', 'sucessfully login');
INSERT INTO tbl_activity VALUES ('21', '0', 'login', '2012-04-20 03:04:04', 'user: asdf and password: asdf (login failed 3)');
INSERT INTO tbl_activity VALUES ('22', '0', 'login', '2012-04-20 03:04:08', 'user: asdf and password: asdf (login failed 2)');
INSERT INTO tbl_activity VALUES ('23', '0', 'login', '2012-04-20 03:04:09', 'user: asdf and password: asdf (login failed 1)');
INSERT INTO tbl_activity VALUES ('24', '11', 'login', '2012-04-20 03:04:17', 'sucessfully login');
INSERT INTO tbl_activity VALUES ('25', '11', 'login', '2012-05-02 05:05:13', 'sucessfully login');
INSERT INTO tbl_activity VALUES ('26', '11', 'login', '2012-05-02 05:05:14', 'sucessfully login');
INSERT INTO tbl_activity VALUES ('27', '11', 'login', '2012-05-31 01:05:40', 'sucessfully login');
INSERT INTO tbl_activity VALUES ('28', '11', 'login', '2012-05-31 01:05:57', 'sucessfully login');
INSERT INTO tbl_activity VALUES ('29', '11', 'login', '2012-05-31 01:05:14', 'sucessfully login');
INSERT INTO tbl_activity VALUES ('30', '11', 'login', '2012-06-10 12:06:42', 'sucessfully login');
INSERT INTO tbl_activity VALUES ('31', '11', 'login', '2012-09-03 07:09:59', 'sucessfully login');

-- ----------------------------
-- Table structure for `tbl_blacklist`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_blacklist`;
CREATE TABLE `tbl_blacklist` (
  `id` int(11) NOT NULL auto_increment,
  `user` int(11) default NULL,
  `date` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_blacklist
-- ----------------------------
INSERT INTO tbl_blacklist VALUES ('1', '5', '2012-04-04 04:04:20', 'Yes');
INSERT INTO tbl_blacklist VALUES ('2', '6', '2012-04-05 01:04:11', 'Yes');

-- ----------------------------
-- Table structure for `tbl_document`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_document`;
CREATE TABLE `tbl_document` (
  `id` int(11) NOT NULL auto_increment,
  `reference_no` varchar(11) NOT NULL,
  `origin_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `category_id` int(11) NOT NULL,
  `date_issue_doc` date NOT NULL,
  `authority_id` int(11) NOT NULL,
  `date_receive_doc` date NOT NULL,
  `line_no` int(11) NOT NULL,
  `folder_no` int(11) NOT NULL,
  `source` varchar(500) default NULL,
  `other` varchar(500) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_document
-- ----------------------------
INSERT INTO tbl_document VALUES ('1', '001', '1', '1', '9', '2011-05-01', '1', '2011-05-01', '7', '0', '001', '001');
INSERT INTO tbl_document VALUES ('2', '002', '2', '2', '1', '2011-05-02', '2', '2011-05-02', '7', '2', '002', '002');
INSERT INTO tbl_document VALUES ('4', '003', '4', '4', '1', '2011-05-07', '4', '2011-05-03', '7', '3', '003', '003');
INSERT INTO tbl_document VALUES ('8', '004', '7', '7', '1', '2011-05-07', '7', '2011-05-04', '7', '4', '004', '004');
INSERT INTO tbl_document VALUES ('10', '004', '9', '7', '2', '2011-05-07', '9', '2011-05-05', '7', '111', '444', '444');
INSERT INTO tbl_document VALUES ('11', '004', '10', '10', '2', '2011-05-07', '10', '2011-05-06', '7', '111', '444', 'áž˜áž·áž“');
INSERT INTO tbl_document VALUES ('12', '', '11', '', '0', '0000-00-00', '11', '0000-00-00', '0', '0', '', '');
INSERT INTO tbl_document VALUES ('13', '', '12', '', '0', '0000-00-00', '12', '0000-00-00', '0', '0', '', '');
INSERT INTO tbl_document VALUES ('14', '', '13', '', '0', '0000-00-00', '13', '0000-00-00', '0', '0', '', '');
INSERT INTO tbl_document VALUES ('15', '', '14', '', '0', '0000-00-00', '14', '0000-00-00', '0', '0', '', '');
INSERT INTO tbl_document VALUES ('16', '', '15', '', '0', '0000-00-00', '15', '0000-00-00', '0', '0', '', '');
INSERT INTO tbl_document VALUES ('17', '', '16', '', '0', '0000-00-00', '16', '0000-00-00', '0', '0', '', '');
INSERT INTO tbl_document VALUES ('18', '', '17', '', '0', '0000-00-00', '17', '0000-00-00', '0', '0', '', '');
INSERT INTO tbl_document VALUES ('19', '234123', '18', 'test a', '9', '2012-03-20', '18', '2012-03-21', '7', '45', '', '');
INSERT INTO tbl_document VALUES ('20', '', '19', '', '0', '0000-00-00', '19', '0000-00-00', '0', '0', '', '');
INSERT INTO tbl_document VALUES ('21', '', '20', 'dfsdf', '0', '0000-00-00', '20', '0000-00-00', '0', '0', '', '');

-- ----------------------------
-- Table structure for `tbl_document_authority`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_document_authority`;
CREATE TABLE `tbl_document_authority` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_document_authority
-- ----------------------------
INSERT INTO tbl_document_authority VALUES ('1', 'Chao Vanserevat');
INSERT INTO tbl_document_authority VALUES ('2', 'Chao Vanserevat');
INSERT INTO tbl_document_authority VALUES ('3', 'Mr. Serevat');
INSERT INTO tbl_document_authority VALUES ('4', 'Chao Vanserevat');
INSERT INTO tbl_document_authority VALUES ('5', '234234');
INSERT INTO tbl_document_authority VALUES ('6', '456456');
INSERT INTO tbl_document_authority VALUES ('7', 'bv');
INSERT INTO tbl_document_authority VALUES ('8', 'bv');
INSERT INTO tbl_document_authority VALUES ('9', '2222');
INSERT INTO tbl_document_authority VALUES ('10', '333');
INSERT INTO tbl_document_authority VALUES ('11', '');
INSERT INTO tbl_document_authority VALUES ('12', '');
INSERT INTO tbl_document_authority VALUES ('13', '');
INSERT INTO tbl_document_authority VALUES ('14', '');
INSERT INTO tbl_document_authority VALUES ('15', '');
INSERT INTO tbl_document_authority VALUES ('16', '');
INSERT INTO tbl_document_authority VALUES ('17', '');
INSERT INTO tbl_document_authority VALUES ('18', 'serevat');
INSERT INTO tbl_document_authority VALUES ('19', '');
INSERT INTO tbl_document_authority VALUES ('20', '');

-- ----------------------------
-- Table structure for `tbl_document_category`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_document_category`;
CREATE TABLE `tbl_document_category` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `description` varchar(1000) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_document_category
-- ----------------------------
INSERT INTO tbl_document_category VALUES ('1', 'Cat1', 'set A ');
INSERT INTO tbl_document_category VALUES ('2', 'Cat2', 'set B & C');
INSERT INTO tbl_document_category VALUES ('6', 'Cat3', 'set D & E');
INSERT INTO tbl_document_category VALUES ('9', 'áž”áŸ’ážšáž—áŸáž‘áž’áž˜áŸ’áž˜ážáž¶', 'áž˜áž·áž“ážŸáž¼ážœáž”áŸ’ážšáž‰áž¶áž”áŸ‹');

-- ----------------------------
-- Table structure for `tbl_document_file`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_document_file`;
CREATE TABLE `tbl_document_file` (
  `id` int(11) NOT NULL auto_increment,
  `document` int(11) NOT NULL,
  `file` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_document_file
-- ----------------------------
INSERT INTO tbl_document_file VALUES ('1', '19', 'ajax-loading.gif');
INSERT INTO tbl_document_file VALUES ('2', '19', 'delete.png');
INSERT INTO tbl_document_file VALUES ('3', '19', 'edit.png');

-- ----------------------------
-- Table structure for `tbl_document_origin`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_document_origin`;
CREATE TABLE `tbl_document_origin` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_document_origin
-- ----------------------------
INSERT INTO tbl_document_origin VALUES ('1', 'áž¢áž„áŸ’áž€áž¶ áž™áž»ážœáž‡áž“áž›áŸ’áž¢');
INSERT INTO tbl_document_origin VALUES ('2', 'Hotel B');
INSERT INTO tbl_document_origin VALUES ('3', 'Organization');
INSERT INTO tbl_document_origin VALUES ('5', '34234');
INSERT INTO tbl_document_origin VALUES ('4', 'Hotel C');
INSERT INTO tbl_document_origin VALUES ('6', '456456');
INSERT INTO tbl_document_origin VALUES ('7', 'Hotel D');
INSERT INTO tbl_document_origin VALUES ('8', 'qwe');
INSERT INTO tbl_document_origin VALUES ('9', '2222');
INSERT INTO tbl_document_origin VALUES ('10', '333');
INSERT INTO tbl_document_origin VALUES ('11', '');
INSERT INTO tbl_document_origin VALUES ('12', '');
INSERT INTO tbl_document_origin VALUES ('13', '');
INSERT INTO tbl_document_origin VALUES ('14', '');
INSERT INTO tbl_document_origin VALUES ('15', '');
INSERT INTO tbl_document_origin VALUES ('16', '');
INSERT INTO tbl_document_origin VALUES ('17', '');
INSERT INTO tbl_document_origin VALUES ('18', 'test');
INSERT INTO tbl_document_origin VALUES ('19', '');
INSERT INTO tbl_document_origin VALUES ('20', '');

-- ----------------------------
-- Table structure for `tbl_folder`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_folder`;
CREATE TABLE `tbl_folder` (
  `id` int(11) NOT NULL auto_increment,
  `number` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_folder
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_info`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_info`;
CREATE TABLE `tbl_info` (
  `id` int(11) NOT NULL auto_increment,
  `company` varchar(50) NOT NULL,
  `address` varchar(500) default NULL,
  `phone` varchar(50) default NULL,
  `website` varchar(50) default NULL,
  `logo` varchar(50) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_info
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_line`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_line`;
CREATE TABLE `tbl_line` (
  `id` int(11) NOT NULL auto_increment,
  `number` varchar(100) NOT NULL,
  `description` varchar(1000) default NULL,
  `standee_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_line
-- ----------------------------
INSERT INTO tbl_line VALUES ('1', '01', 'file a', '1');
INSERT INTO tbl_line VALUES ('2', '02', 'file b', '7');
INSERT INTO tbl_line VALUES ('7', '02', 'file c', '1');

-- ----------------------------
-- Table structure for `tbl_module`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_module`;
CREATE TABLE `tbl_module` (
  `id` int(11) NOT NULL auto_increment,
  `module_id` varchar(10) NOT NULL,
  `module` varchar(20) NOT NULL,
  `href` varchar(20) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_module
-- ----------------------------
INSERT INTO tbl_module VALUES ('1', 's_doc', 'Search Document', 'search-document', '1');
INSERT INTO tbl_module VALUES ('2', 'doc', 'Document', 'document', '2');
INSERT INTO tbl_module VALUES ('3', 'doc_cat', 'Document Category', 'document-category', '3');
INSERT INTO tbl_module VALUES ('4', 'standee', 'Standee', 'standee', '4');
INSERT INTO tbl_module VALUES ('5', 'line', 'Line', 'line', '5');
INSERT INTO tbl_module VALUES ('6', 'man_user', 'Manage User', 'manage-user', '6');
INSERT INTO tbl_module VALUES ('7', 'rep', 'Report', 'report', '7');
INSERT INTO tbl_module VALUES ('8', 'con', 'Config', 'config', '8');

-- ----------------------------
-- Table structure for `tbl_standee`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_standee`;
CREATE TABLE `tbl_standee` (
  `id` int(11) NOT NULL auto_increment,
  `number` varchar(50) NOT NULL,
  `description` varchar(1000) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_standee
-- ----------------------------
INSERT INTO tbl_standee VALUES ('1', '001', 'Red');
INSERT INTO tbl_standee VALUES ('7', '003', 'Blue');
INSERT INTO tbl_standee VALUES ('6', '002', 'Yellow');
INSERT INTO tbl_standee VALUES ('10', '004', 'pink');
INSERT INTO tbl_standee VALUES ('11', '005', 'green');

-- ----------------------------
-- Table structure for `tbl_user`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(20) NOT NULL,
  `password` varchar(300) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO tbl_user VALUES ('11', 'serevat', 'bda667fa7f3207e051b5933d514b8d68', 'serevat', 'bong');
INSERT INTO tbl_user VALUES ('12', 'dara', '81dc9bdb52d04dc20036dbd8313ed055', '', '');
