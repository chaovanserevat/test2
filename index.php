<?php

session_start();
include ("connection.php");

$logout = isset ($_SESSION["user-logout"]) ? $_SESSION["user-logout"] : -1;

if ($_SESSION['user-login'] < 1 || $_SESSION['user-login'] == -1 || $logout == 1) {
	$_SESSION["user-logout"] = -1;
	header ("location: login.php");
	exit ();
}

$user_fullname = $_SESSION["fullname"];
$today = date("F j, Y, g:i a");

$select_module = $connection->query ("SELECT * FROM tbl_module");
$modules = array ();
while ($get_module = $select_module->fetch_assoc ()) {
	$modules[] = $get_module;
}

?>

<html>
	<head>
		<title>Document Management</title>
		<link type="text/css" rel="stylesheet" href="js/jquery-ui-1.8.14.custom/css/custom-theme/jquery-ui-1.8.14.custom.css" />
		<link type="text/css" rel="stylesheet" href="css/document.css" />
		<img class="ajax-loading" src="<?= $_base_url ?>/img/ajax-loading.gif" alt="" style="display: none; border: 0 none;" />
		<script type="text/javascript" src="<?= $_base_url ?>/js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="<?= $_base_url ?>/js/plugin/jquery.nked.layout.js"></script>
		<script type="text/javascript" src="<?= $_base_url ?>/js/jquery-ui-1.8.14.custom/js/jquery-ui-1.8.14.custom.min.js"></script>
		<style type="text/css">

			html, body {
				margin: 0px;
				padding: 0px;
			}

			* {
				font-family: Consolas, "Lucida Console", "Trebuchet MS";
				font-size: 12px;
			}

			#container > div {
				border-bottom: 1px solid #CCCCCC;
			}

			#top {
				height: 70px;
			}

			#middle {
			}

			#bottom {
				height: 100px;
				text-align: center;
				line-height: 30px;
			}

			#menubar-container #menubar-navigation span {
			    background-image: url("<?= base_url() ?>/img/header/header.gif");
				border: medium none;
				color: #FFFFFF;
				display: block;
				font-size: 12px;
				padding: 70px 10px 3px;
				margin-right: 10px;
				cursor: pointer
			}

			#menubar-container #menubar-navigation a:hover {
				background-position: -529px -112px;
			}

			#search-document { background-position: -12px -39px; }
			#document { background-position: 541px 261px; }
			#document-category { background-position: 424px -39px; }
			#standee { background-position: 392px -39px; }
			#line { background-position: 381px -39px; }
			#manage-user { background-position: 263px -39px; }
			#report { background-position: -189px -184px; }
			#config { background-position: -43px -184px; }

			* { text-decoration: none; }


			.link-edit img, .link-delete img , .link-print img {
				width: 16px;
				height: 16px;
			}

			.nk-tbl-data tbody tr:nth-child(odd) { background: #EDF7F8 }
			.nk-tbl-data tbody tr:nth-child(even) { background: #F8F9ED }
			.nk-tbl-data tbody > tr:hover { background: #FFF }

			.fd-ckb-cked { background-color: #CEFFCD !important; }

			.star-requiredx { color: red; }
			.requiredx-needed { border: 1px solid #FF0000; }
			
			.focus-color { background-color: #F1F1F1; }

		</style>
	</head>
	<body>
		<div id="container">
			<div id="top" style="">
				<span style="position: absolute; top: 5px; left: 5px; color: #DE8745; font-size: 20px; font-weight: bold">Document Management</span>
				<table id="menubar-container" align="right">
					<tbody>
						<tr>
							<? foreach ($modules AS $module) : ?>
							<td id="menubar-navigation">
								<span class="menu-top" id="<?= $module["href"] ?>"><?= $module["module"] ?></span>
							</td>
							<? endforeach; ?>
						</tr>
					</tbody>
				</table>
			</div>
			<div id="middle" style="width: 100%; overflow-y: auto;">
				<div id="content" style="background-color: white; border: 0 none; padding: 0px 20px;"></div>
			</div>
			<div id="bottom">
				<div style="font-size: 12px;">
					<span style="margin-right: 50px;"><?= $today ?></span>
					Welcome <b><?= $user_fullname ?></b>! | <a href="<?= base_url() ?>logout.php">Logout</a>
				</div>
				<p style="color: #777777;">Nked - Document Management Power by <a href="http://sursdey.com">bv<a></p>
			</div>
		</div>
		<script type="text/javascript">

			jQuery.extend({
				ajax_loading: function(args) {
					var loading = $(".ajax-loading").clone()
						.attr("class", "ajax-loading-clone").show();
					if (!args) return loading;
					else if (args == "hide") {
						$(".ajax-loading-clone").remove();
					}
				}
			});

			$(function() {

				$("#middle").middleHeight();
				$("#content").load("content/document/document.php");

				$(".menu-top").click(function() {
					var id = $(this).attr("id");
					$("#content").load("content/" + id + "/" + id + ".php");
				});


				$.fn.existx = function(){ return this.length > 0; }

				$.fn.clearForm = function() {
					return this.each(function() {
						var type = this.type, tag = this.tagName.toLowerCase();

						if (tag == "form")
							return $(":input", this).clearForm();
						if (type == "text" || type == "password" || tag == "textarea")
							this.value = "";
						else if (type == "checkbox" || type == "radio")
							this.checked = false;
						else if (tag == "select")
							this.selectedIndex = -1;
					});
				};

				$.fn.tickCheckbox = function() {
					var _checkbox = $(".nk-tbl-data input:checkbox");
					_checkbox.addClass("fd-ckb");

					return _checkbox.each(function() {
						$(this).closest("tr").addClass("tr-fd-ckb");
					});
				}

				$.fn.ifCheck = function() {
					var _checkbox = $(".fd-ckb:checked");

					$(".tr-fd-ckb").removeClass("fd-ckb-cked");

					return _checkbox.each(function() {
						$(this).closest("tr").addClass("fd-ckb-cked");
					});
				}

				$(".tr-fd-ckb").live("click", function() {
					var _checkbox = $(this).find(".fd-ckb");

					var _status = _checkbox.attr("checked") ? false : true;

					_checkbox.attr("checked", _status);

					$.ifCheck();
				});

				$(".fd-ckb").live("click", function() {
					var _status = $(this).attr("checked") ? false : true;
					$(this).attr("checked", _status);

					$.ifCheck();
				});

				$.fn.noSpecialChar = function() {

					return $(this).each (function() {

						$(this).addClass("input-no-special-char");

					});

				}

				var _old_ = "";

				$(".input-no-special-char, .no-special-char").live("keyup", function() {
					var inputVal = $(this).val();
					var characterReg = /^[a-zA-Z0-9_]*$/;

					if(!characterReg.test(inputVal)) {
						$(this).val(_old_);
						alert("No special characters allowed.");
					}
				})
				.live("keydown", function() {
					_old_ = $.trim ($(this).val());
				});

				$.fn.checkRequiredx = function() {

					var _form = $(".formx-entry");

					return _form.each(function() {

						_requiredx = [];

						$(this).find(".requiredx").each(function(i) {
							if ($(this).val() == "") {
								_requiredx[i] = $(this);
								$(this).addClass("requiredx-needed");
							}
							else $(this).removeClass("requiredx-needed");
						});

						if (_requiredx.length > 0) {
							return false;
						}
					});

				}
				
				$.fn.validatex = function(event) {
				
					$.extend({
						checkRequiredx: $.fn.checkRequiredx
					});
					
					$.checkRequiredx();
					
					if ($(".requiredx-needed").existx() > 0) {
						$(".requiredx-needed[value='']:first").focus();
						// event.stopImmediatePropagation();
						return false;
					}
					
				}

				$.extend({
					tickCheckbox: $.fn.tickCheckbox,
					ifCheck: $.fn.ifCheck,
					locationx: $.fn.locationx,
					validatex: $.fn.validatex
				});
				
				$(".focus").live("focus", function() {
					$(".focus").removeClass("focus-color");
					$(this).addClass("focus-color")
				});

			});

			$(window).resize(function() {
				$("#middle").middleHeight();
			});

		</script>
	</body>
</html>