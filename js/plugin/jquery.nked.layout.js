(function($) {

	jQuery.fn.middleHeight = function(options) {
		// var opts = $.extend({}, $.fn.bvLayout.defaults, options);
		return this.each(function() {
			$(this).css({
				height: getTopBottomHeight()
			});
		});
	};
	
	function getTopBottomHeight() {
		var top_height = $("#top").outerHeight();
		var bottom_height = $("#bottom").outerHeight();
		var middle_height = getDocumentHeight() - (top_height + bottom_height) - 25;
		return middle_height + "px";
	}
	
	function getDocumentHeight() {
		return $(window).height() - 2;
	}

})(jQuery);