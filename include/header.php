<html>
	<head>
		<title>Document Management</title>
		<link type="text/css" rel="stylesheet" href="../../../js/jquery-ui-1.8.14.custom/css/custom-theme/jquery-ui-1.8.14.custom.css" />
		<link type="text/css" rel="stylesheet" href="../../../css/document.css" />
		<img class="ajax-loading" src="<?= base_url() ?>/img/ajax-loading.gif" alt="" style="display: none; border: 0 none;" />
		<script type="text/javascript" src="../../../js/jquery-1.5.1.min.js"></script>
		<script type="text/javascript" src="../../../js/plugin/jquery.nked.layout.js"></script>
		<script type="text/javascript" src="../../../js/jquery-ui-1.8.14.custom/js/jquery-ui-1.8.14.custom.min.js"></script>
		<style type="text/css">

			html, body {
				margin: 0px;
				padding: 0px;
			}

			* {
				font-family: Consolas, "Lucida Console", "Trebuchet MS";
				font-size: 12px;
			}

			#container > div {
				border-bottom: 1px solid #CCCCCC;
			}

			#top {
				height: 70px;
			}

			#middle {
			}

			#bottom {
				height: 100px;
				text-align: center;
				line-height: 30px;
			}

			#menubar-container #menubar-navigation span {
			    background-image: url("<?= base_url() ?>/img/header/header.gif");
				border: medium none;
				color: #FFFFFF;
				display: block;
				font-size: 12px;
				padding: 70px 10px 3px;
				margin-right: 10px;
				cursor: pointer
			}

			#menubar-container #menubar-navigation a:hover {
				background-position: -529px -112px;
			}

			#search-document { background-position: -12px -39px; }
			#document { background-position: 541px 261px; }
			#document-category { background-position: 424px -39px; }
			#standee { background-position: 392px -39px; }
			#line { background-position: 381px -39px; }
			#manage-user { background-position: 263px -39px; }
			#report { background-position: -189px -184px; }
			#config { background-position: -43px -184px; }

			* { text-decoration: none; }


			.link-edit img, .link-delete img , .link-print img {
				width: 16px;
				height: 16px;
			}

			.nk-tbl-data tbody tr:nth-child(odd) { background: #EDF7F8 }
			.nk-tbl-data tbody tr:nth-child(even) { background: #F8F9ED }
			.nk-tbl-data tbody > tr:hover { background: #FFF }

			.fd-ckb-cked { background-color: #CEFFCD !important; }

			.star-requiredx { color: red; }
			.requiredx-needed { border: 1px solid #FF0000; }
			
			.focus-color { background-color: #F1F1F1; }

		</style>
	</head>
	<body>
		<div id="container">
			<div id="top" style="">
				<span style="position: absolute; top: 5px; left: 5px; color: #DE8745; font-size: 20px; font-weight: bold">Document Management</span>
				<table id="menubar-container" align="right">
					<tbody>
						<tr>
							<? foreach ($modules AS $module) : ?>
							<td id="menubar-navigation">
								<span class="menu-top" id="<?= $module["href"] ?>"><?= $module["module"] ?></span>
							</td>
							<? endforeach; ?>
						</tr>
					</tbody>
				</table>
			</div>
			<div id="middle" style="width: 100%; overflow-y: auto;">
				<div id="content" style="background-color: white; border: 0 none; padding: 0px 20px;">