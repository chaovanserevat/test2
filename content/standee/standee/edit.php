<?php 

include ("../../../connection.php");
$edit_id = $_POST["id"]; 
$select = $connection->query("SELECT number, description FROM tbl_standee WHERE id = '$edit_id'");
$data = $select->fetch_assoc();

?>

<div>
    <p>
        <label for="txt-standee-number-edit">Category Name</label>&nbsp;<span style="color: red;">*</span><br />
        <input id="txt-standee-number-edit" class="input new_theme_textbox ui-corner-all focus" type="text" value="<?= $data["number"] ?>" maxlength="25" />
    </p>
    <p>
        <label for="txt-standee-description-edit">Description</label>&nbsp;<span style="color: red;">*</span><br />
		<textarea id="txt-standee-description-edit" class="ui-corner-all focus" style="width: 480px; height: 120px;"><?= $data["description"] ?></textarea>
    </p>
	<br />
	<span>Fields has asterisk(<span style="color: red;">*</span>) mark are required</span>
</div>

<script type="text/javascript">
$("#txt-standee-number-edit").select().keydown(function(e) {
    if (e.keyCode == "13") {
        $(":button:contains(Save)").trigger("click");
    }
});

function save_standee() {
    if ($("#txt-standee-number-edit").val() == "") {
        $("#txt-standee-number-edit").addClass("ui-state-error");
        $("#msg-status-1").show()
        .css({
            color: "red",
            padding: "4px 8px"
        })
        .html("Data required!");
    } else {
        $("#txt-standee-number-edit").removeClass("ui-state-error");
        $("#msg-status-1").hide();
    }
    if ($("#txt-standee-number-edit").hasClass("ui-state-error")) {
        $("#txt-standee-number-edit").focus(); return;
    }
	
    $.ajax({
        url: "content/standee/standee/save.php",
        data: {
            id: <?= $edit_id ?>,
            number: $("#txt-standee-number-edit").val(),
            description: $("#txt-standee-description-edit").val()
        },
        type: "post",
        dataType: "html",
        success: function(result) {
            $("#tbl-standee tbody").load("content/standee/standee/reload.php", function() {
                if (result == "success") {
                    $("#table-status")
                        .removeClass()
                        .addClass("ui-state-highlight")
                        .html("A user group has been saved successfully!")
                        .stop(true, true).fadeTo(0, 1).hide()
                        .slideDown("normal").delay(2000).fadeOut("slow");
                }
            });
            $("#dlg-standee-edit").dialog("close");
        },
        error: function(xhr, status, ex) {
            var error_status = $(xhr.responseText).find("p:contains(Error)").text().split(/: /);
            var error_code = parseInt(error_status[1], 10);
            var error_type = "";
            switch (error_code) {
                case 1062:
                    error_type = "Existed!";
                    break;
            }
            $("#entry-status")
                .removeClass()
                .addClass("ui-state-error").html("")
                .html("Error: Data " + error_type)
                .stop(true, true).fadeTo(0, 1).hide()
                .slideDown("normal").delay(2000).fadeOut("slow");
            $("#dlg-standee-edit").dialog("close");
        }
    });
}
</script>