<?php 

include ("../../../connection.php");
$select = $connection->query("SELECT id, number FROM tbl_standee");
$standees = array ();
while ($standee = $select->fetch_assoc()) :
	$standees[] = $standee;
endwhile

?>

<div>
    <p>
        <label for="select-standee-number">Standee Number</label>&nbsp;<span style="color: red;">*</span><br />
        <select id="select-standee-number" class="input new_theme_textbox ui-corner-all">
            <option>Select Standee</option>
            <?php foreach ($standees as $standee) : ?>
            <option value="<?= $standee["id"] ?>"><?= $standee["number"] ?></option>
            <? endforeach; ?>
        </select>
    </p>
    <p>
        <label for="select-standee-line-number">Line Number</label>&nbsp;<span style="color: red;">*</span><br />
        <input id="select-standee-line-number" class="input new_theme_textbox ui-corner-all focus" type="text" />
    </p>
    <p>
        <label for="txt-standee-line-description">Description</label>&nbsp;<span style="color: red;">*</span><br />
		<textarea id="txt-standee-line-description" class="ui-corner-all focus" style="width: 480px; height: 120px;"></textarea>
    </p>
</div>

<script type="text/javascript">
$("#select-standee-line-number").focus().keydown(function(e) {
    if (e.keyCode == "13") {
        $(":button:contains(Save)").trigger("click");
    }
});

function save_standee_line() {
    if ($("#select-standee-line-number").val() == "") {
        $("#select-standee-line-number").addClass("ui-state-error");
        $("#msg-status-1").show()
        .css({
            color: "red",
            padding: "4px 8px"
        })
        .html("Data required!");
    } else {
        $("#select-standee-line-number").removeClass("ui-state-error");
        $("#msg-status-1").hide();
    }
    if ($("#select-standee-line-number").hasClass("ui-state-error")) {
        $("#select-standee-line-number").focus(); return;
    }

    $.ajax({
        url: "content/line/line/save.php",
        data: {
            id: 0,
            number: $("#select-standee-line-number").val(),
            description: $("#txt-standee-line-description").val(),
            standee: $("#select-standee-number").val()
        },
        type: "post",
        dataType: "html",
        success: function(result) {
            $("#tbl-standee-line tbody").load("content/line/line/reload.php", function() {
                if (result == "success") {
                    $("#table-status")
                        .removeClass()
                        .addClass("ui-state-highlight")
                        .html("A standee line has been saved successfully!")
                        .stop(true, true).fadeTo(0, 1).hide()
                        .slideDown("normal").delay(2000).fadeOut("slow");
                }
            });
            $("#dlg-standee-line-new").dialog("close");
        },
        error: function(xhr, status, ex) {
            var error_status = $(xhr.responseText).find("p:contains(Error)").text().split(/: /);
            var error_code = parseInt(error_status[1], 10);
            var error_type = "";
            switch (error_code) {
                case 1062:
                    error_type = "Existed!";
                    break;
            }
            $("#table-status")
                .removeClass()
                .addClass("ui-state-error").html("")
                .html("Error: Data " + error_type)
                .stop(true, true).fadeTo(0, 1).hide()
                .slideDown("normal").delay(2000).fadeOut("slow");
            $("#dlg-standee-line-new").dialog("close");
        }
    });
}
</script>