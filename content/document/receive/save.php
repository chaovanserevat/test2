<?php

include ("../../../connection.php");

$data = isset ($_POST["data"]) ? $_POST["data"] : "";
$type = isset ($_POST["type"]) ? $_POST["type"] : "";
$base_url = base_url() . "img/upload/document/";

function _remove_uploaded_file($_file = "")
{
	$remove = unlink ("../../../img/upload/document/$_file");
}

if ($type == "upload-file")
{
	$success_paragraph = "";
	$uploaddir = "../../../img/upload/document/";
	$file = $uploaddir . basename($_FILES["file"]["name"]);
	$file_name = $_FILES["file"]["name"];
	$ext = explode(".", $file_name);

	if (move_uploaded_file($_FILES["file"]["tmp_name"], $file)) {
		$message = "<p id='{$ext[0]}' style='margin: 10px 0px 0px 0px;'>@&nbsp;<a class='uploaded-file' style='color: blue; padding: 5px;' href='$base_url$file_name' target='_blank'>Uploaded file: $file_name</a>";
		$message .= "&nbsp;<span id='$file_name' name='{$ext[0]}' class='span-cancel-upload' style='color: red; cursor: pointer;'>cancel</span><br /></p>";
		$message .= "<input class='hidden-file-link' type='hidden' value='$file_name' />";
	}
	else $message = "error";

	?>
	<script type='text/javascript'> parent.show_message("<?= $message ?>") </script>
	<?

	exit ();
}

if ($type == "cancel-upload")
{
	$uploaded_file = isset ($_POST["file"]) ? $_POST["file"] : "";
	_remove_uploaded_file ($uploaded_file);
	
	echo $remove ? 1 : 0;

	exit ();
}

if ($data[0] == 0)
{
	// origin
	$save_origin = $connection->query("INSERT INTO tbl_document_origin SET name = '{$data[2]}'");
	if ($save_origin) {
		$max_origin = $connection->query("SELECT MAX(id) as id FROM tbl_document_origin");
		$max_document = $max_origin->fetch_assoc();
	}

	//authority
	$save_authority = $connection->query("INSERT INTO tbl_document_authority SET name = '{$data[6]}'");
	if ($save_authority) {
		$max_authority = $connection->query("SELECT MAX(id) as id FROM tbl_document_authority");
		$max_authority = $max_authority->fetch_assoc();
	}
	
	$in_ou = $data[13];
	
	// document_categories
	$query = $connection->query("
									INSERT INTO tbl_document
									SET 
										reference_no = '{$data[1]}',
										origin_id = '{$max_document["id"]}',
										name = '{$data[3]}',
										category_id = '{$data[4]}',
										date_issue_doc = '{$data[5]}',
										authority_id = '{$max_authority["id"]}',
										date_receive_doc = '{$data[7]}',
										folder_no = '{$data[8]}',
										line_no = '{$data[10]}',
										source = '{$data[11]}',
										other = '{$data[12]}',
										in_out = '$in_ou'
								");

	$query = $connection->query("SELECT MAX(id) AS 'id' FROM tbl_document");
	$max_id = $query->fetch_assoc();
	
	if (count ($data[14]) > 0 && $data[14] != "")
	{
		foreach ($data[14] AS $uploaded_file) {
			$query = $connection->query("INSERT INTO tbl_document_file SET document = '{$max_id["id"]}', file = '$uploaded_file'");
		}
	}

	exit ();
}

else
{
	// origin
	$save_origin = $connection->query("UPDATE tbl_document_origin SET name = '{$data[2]}' WHERE id = '{$data[3]}'");
	if ($save_origin) {
		$max_origin = $connection->query("SELECT MAX(id) as id FROM tbl_document_origin");
		$max_document = $max_origin->fetch_assoc();
	}

	//authority
	$save_authority = $connection->query("INSERT INTO tbl_document_authority SET name = '{$data[7]}' WHERE id = '{$data[8]}'");
	
	if ($save_authority) {
		$max_authority = $connection->query("SELECT MAX(id) as id FROM tbl_document_authority");
		$max_authority = $max_authority->fetch_assoc();
	}

	$_in_ou = $data[15];
		
	// document_categories
	$query = $connection->query("
									UPDATE tbl_document
									SET
										reference_no = '{$data[1]}',
										name = '{$data[3]}',
										category_id = '{$data[5]}',
										date_issue_doc = '{$data[6]}',
										date_receive_doc = '{$data[9]}',
										folder_no = '{$data[10]}',
										line_no = '{$data[12]}',
										source = '{$data[13]}',
										other = '{$data[14]}',
										in_out = '$_in_ou'
									WHERE id = '{$data[0]}'
	");
	
	$query = $connection->query("SELECT MAX(id) AS 'id' FROM tbl_document");
	$max_id = $query->fetch_assoc();
	
	if (count ($data[16]) > 0 && $data[16] != "")
	{
		$connection->query ("DELETE FROM tbl_document_file WHERE document = '{$max_id["id"]}'");
		
		foreach ($data[16] AS $uploaded_file) 
		{
			$query = $connection->query("INSERT INTO tbl_document_file SET document = '{$max_id["id"]}', file = '$uploaded_file'");
		}
	}
}

$connection->close();

?>