<?php 

include ("../../../connection.php");
$edit_id = $_POST["id"];

$select = 	"SELECT doc.*, ori.id as origin_id, ori.name as origin, cat.`name` as category, auth.id as authority_id, auth.`name` as authority, stan.id as standee_id, stan.number as standee, line.id as line_id, line.number as line
				FROM tbl_document doc
				INNER JOIN tbl_document_origin ori ON ori.id = doc.origin_id
				INNER JOIN tbl_document_category cat ON cat.id = doc.category_id
				INNER JOIN tbl_document_authority auth ON auth.id = doc.authority_id
				INNER JOIN tbl_line line ON line.id = doc.line_no
				INNER JOIN tbl_standee stan ON stan.id = line.standee_id
				WHERE doc.id = '$edit_id'
			";	
			
$data = $connection->query($select);
$documents = $data->fetch_assoc();

$select_categories = $connection->query("SELECT * FROM tbl_document_category");
$categories = array ();
while ($category = $select_categories->fetch_assoc()) :
	$categories[] = $category;
endwhile;

$select_standees = $connection->query("SELECT * FROM tbl_standee");
$standees = array ();
while ($standee = $select_standees->fetch_assoc()) :
	$standees[] = $standee;
endwhile;

?>

<div>
	<table>
		<tr>
			<td style="width: 300px;">
				<p>
					<label for="txt-document-receive-in-ou">Document In/Out</label>&nbsp;<span style="color: red;">*</span><br />
					<select id="txt-document-receive-in-ou" class="input ui-corner-all focus">
						<option></option>
						<option value="in" <?= $documents["in_out"] == "in" ? "selected" : "" ?>>In</option>
						<option value="out" <?= $documents["in_out"] == "out" ? "selected" : "" ?>>Out</option>
					</select>
				</p>
				<p>
					<label for="txt-document-receive-reference-number-edit">Reference Number</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-reference-number-edit" class="input new_theme_textbox ui-corner-all focus" type="text" value="<?= $documents["reference_no"] ?>" />
				</p>
				<p>
					<label for="txt-document-receive-origin-edit">Document Origin</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-origin-edit" class="input new_theme_textbox ui-corner-all focus" type="text" value="<?= $documents["origin"] ?>" />
					<input id="document-receive-hidden-origin-edit" type="hidden" value="<?= $documents["origin_id"] ?>" />
				</p>
				<p>
					<label for="txt-document-receive-name-edit">Document Name</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-name-edit" class="input new_theme_textbox ui-corner-all focus" type="text" value="<?= $documents["name"] ?>" />
				</p>
				<p>
					<label for="txt-document-receive-category-edit">Document Category</label>&nbsp;<span style="color: red;">*</span><br />
					<select id="txt-document-receive-category-edit" class="input new_theme_textbox ui-corner-all focus">
						<? foreach($categories as $category) : ?>
						<option value="<?= $category["id"] ?>" <?= $category["id"] == $documents["category_id"] ? "selected" : "" ?>><?= $category["name"] ?></option>
						<? endforeach ?>
					</select>
				</p>
			</td>
			<td style="width: 300px; vertical-align: top;">
				<p>
					<label for="txt-document-receive-date-issue-edit">Date of Document Issue</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-date-issue-edit" class="calendar input new_theme_textbox ui-corner-all focus" type="text" value="<?= $documents["date_issue_doc"] ?>" />
				</p>
				<p>
					<label for="txt-document-receive-authority-edit">Document Authority</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-authority-edit" class="input new_theme_textbox ui-corner-all focus" type="text" value="<?= $documents["authority"] ?>" />
					<input id="document-receive-hidden-authority-edit" type="hidden" value="<?= $documents["authority_id"] ?>" />
				</p>
				<p>
					<label for="txt-document-receive-date-receive-edit">Date of Receive Document</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-date-receive-edit" class="calendar input new_theme_textbox ui-corner-all focus" type="text" value="<?= $documents["date_receive_doc"] ?>" />
				</p>
				<p>
					<label for="txt-document-receive-folder-number-edit">Folder Number</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-folder-number-edit" class="input new_theme_textbox ui-corner-all focus" type="text" value="<?= $documents["folder_no"] ?>" />
				</p>
			</td>
			<td style="width: 300px; vertical-align: top;">
				<p>
					<label for="txt-document-receive-standee-number-edit">Standee Number</label>&nbsp;<span style="color: red;">*</span><br />
					<select id="txt-document-receive-standee-number-edit" class="input new_theme_textbox ui-corner-all focus">
						<? foreach($standees as $standee) : ?>
						<option value="<?= $standee["id"] ?>" <?= $standee["id"] == $documents["standee_id"] ? "selected" : "" ?>><?= $standee["number"] ?></option>
						<? endforeach ?>
					</select>
				</p>
				<p>
					<label for="txt-document-receive-line-number-edit">Standee Line Number</label>&nbsp;<span style="color: red;">*</span><br />
					<select id="txt-document-receive-line-number-edit" class="input new_theme_textbox ui-corner-all focus">
						<option value="<?= $documents["line_id"] ?>"><?= $documents["line"] ?></option>
					</select>
				</p>
				<p>
					<label for="txt-document-receive-source-edit">Source</label><br />
					<input id="txt-document-receive-source-edit" class="input new_theme_textbox ui-corner-all focus" type="text" value="<?= $documents["source"] ?>" />
				</p>
				<p>
					<label for="txt-document-receive-other-edit">Other</label><br />
					<input id="txt-document-receive-other-edit" class="input new_theme_textbox ui-corner-all focus" type="text" value="<?= $documents["other"] ?>" />
				</p>
			</td>
		</tr>
	</table>
	
	<p><button id="btn-document-upload-file">Upload File</button></p>
	
	<form id="upload-form" action="<?= base_url() ?>content/document/receive/save.php" method="post" target="uploadiframe" enctype="multipart/form-data">
		<input type="hidden" name="type" value="upload-file" />
	</form>
	<iframe id="uploadiframe" name="uploadiframe" style="width: 0px; height: 0px; border: 0 none;"></iframe>
	<p id="p-uploaded-file"></p>
	
	<br /><br />
	<span>Fields has asterisk(<span style="color: red;">*</span>)mark are required</span>
</div>

<script type="text/javascript">

	$(function() {
	
		$("button").button();
	
		/* calendar ==================================================================*/

        $(".calendar").datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true, changeYear: true, gotoCurrent: true
        });
        $(".ui-datepicker").css("font-size", "12px");
		$("#ui-datepicker-div").css("z-index", 2000);

		/* end calendar ==============================================================*/
		
		$("#txt-document-receive-standee-number-edit").change(function() {
			var standee = $(this).val();
			$("#txt-document-receive-line-number-edit").load("content/document/receive/get_standee_line.php", {
				"standee": standee
			});
		});
		
		$("#btn-document-upload-file").click(function() {
			$("<input id='file' name='file' type='file' />").css({
				position: "absolute",
				display: "none"
			})
			.appendTo("#upload-form")
			.trigger("click")
			.change(function() {
				$("#upload-form").submit();
			});
			
		});
		
		$(".span-cancel-upload").live("click", function() {
			var afile = $(this).attr("name");
			
			$.ajax ({
				url: "<?= base_url() ?>content/document/receive/save.php",
				type: "post",
				data: { type: "cancel-upload", file: $(this).attr("id") },
				success: function(result) {
					if (result == 1) $("#" + afile).remove();
					else alert("Cannot cancel this file..");
				}
			});
		});
	});
	
	function show_message(message) {
		$("#p-uploaded-file").append(message);
	}

	function save_edit_document() {
		var uploaded_file = [];
		
		$(".uploaded-file").parent().find("span").each(function(i) {
			uploaded_file[i] = $(this).attr("id");
		});
		
		var data = [
			<?= $edit_id ?>,										//	0	
			$("#txt-document-receive-reference-number-edit").val(),	//	1
			$("#txt-document-receive-origin-edit").val(),			//	2	
			$("#document-receive-hidden-origin-edit").val(),		//	3
			$("#txt-document-receive-name-edit").val(),				//	4
			$("#txt-document-receive-category-edit").val(),			//	5
			$("#txt-document-receive-date-issue-edit").val(),		//	6
			$("#txt-document-receive-authority-edit").val(),		//	7	
			$("#document-receive-hidden-authority-edit").val(),		//	8
			$("#txt-document-receive-date-receive-edit").val(),		//	9
			$("#txt-document-receive-folder-number-edit").val(),	//	10
			$("#txt-document-receive-standee-number-edit").val(),	//	11
			$("#txt-document-receive-line-number-edit").val(),		//	12
			$("#txt-document-receive-source-edit").val(),			//	13
			$("#txt-document-receive-other-edit").val(),				//	14
			$("#txt-document-receive-in-ou").val(),						//	15
			uploaded_file		
		];

		$.ajax({
			url: "content/document/receive/save.php",
			data: {
				data: data
			},
			type: "post",
			dataType: "html",
			success: function(result) {
				$("#tbl-document-receive tbody").load("content/document/receive/reload.php", function() {
					if (result == "success") {
						$("#table-status")
							.removeClass()
							.addClass("ui-state-highlight")
							.html("A user group has been saved successfully!")
							.stop(true, true).fadeTo(0, 1).hide()
							.slideDown("normal").delay(2000).fadeOut("slow");
					}
				});
				$("#dlg-document-receive-new").dialog("close");
			},
			error: function(xhr, status, ex) {
				var error_status = $(xhr.responseText).find("p:contains(Error)").text().split(/: /);
				var error_code = parseInt(error_status[1], 10);
				var error_type = "";
				switch (error_code) {
					case 1062:
						error_type = "Existed!";
						break;
				}
				$("#table-status")
					.removeClass()
					.addClass("ui-state-error").html("")
					.html("Error: Data " + error_type)
					.stop(true, true).fadeTo(0, 1).hide()
					.slideDown("normal").delay(2000).fadeOut("slow");
				$("#dlg-document-receive-new").dialog("close");
			}
		});
	}
	
</script>