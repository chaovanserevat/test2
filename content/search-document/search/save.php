<?php

include ("../../../connection.php");
$data = $_POST["data"];

if ($data[0] == 0) {
	// origin
	$save_origin = $connection->query("INSERT INTO tbl_document_origin SET name = '{$data[2]}'");
	if ($save_origin) {
		$max_origin = $connection->query("SELECT MAX(id) as id FROM tbl_document_origin");
		$max_document = $max_origin->fetch_assoc();		
	}
	
	//authority
	$save_authority = $connection->query("INSERT INTO tbl_document_authority SET name = '{$data[6]}'");
	if ($save_authority) {
		$max_authority = $connection->query("SELECT MAX(id) as id FROM tbl_document_authority");
		$max_authority = $max_authority->fetch_assoc();
	}
			
	// document_categories
	$query = $connection->query("INSERT INTO tbl_document 
									SET reference_no = '{$data[1]}', 
									origin_id = '{$max_document["id"]}', 
									name = '{$data[3]}', 
									category_id = '{$data[4]}', 
									date_issue_doc = '{$data[5]}',
									authority_id = '{$max_authority["id"]}',
									date_receive_doc = '{$data[7]}',
									folder_no = '{$data[8]}',
									line_no = '{$data[10]}',
									source = '{$data[11]}',
									other = '{$data[12]}'
								");
}
else {
	// origin
	$save_origin = $connection->query("UPDATE tbl_document_origin SET name = '{$data[2]}' WHERE id = '{$data[3]}'");
	if ($save_origin) {
		$max_origin = $connection->query("SELECT MAX(id) as id FROM tbl_document_origin");
		$max_document = $max_origin->fetch_assoc();		
	}
	
	//authority
	$save_authority = $connection->query("INSERT INTO tbl_document_authority SET name = '{$data[7]}' WHERE id = '{$data[8]}'");
	if ($save_authority) {
		$max_authority = $connection->query("SELECT MAX(id) as id FROM tbl_document_authority");
		$max_authority = $max_authority->fetch_assoc();
	}
			
	// document_categories
	$query = $connection->query("UPDATE tbl_document 
									SET reference_no = '{$data[1]}', 
									name = '{$data[3]}', 
									category_id = '{$data[5]}', 
									date_issue_doc = '{$data[6]}',
									date_receive_doc = '{$data[9]}',
									folder_no = '{$data[10]}',
									line_no = '{$data[12]}',
									source = '{$data[13]}',
									other = '{$data[14]}'
								WHERE id = '{$data[0]}'	
								");
}
		
// if ($query) :
	// include ("reload.php");
// endif;

$connection->close();

?>