<?php

include ("../../../connection.php");
$select = "SELECT * FROM tbl_document_category";
$result = $connection->query($select);
$document_categories = array ();
while ($document_category = $result->fetch_assoc()) :
	$document_categories[] = $document_category;
endwhile;

$select = "SELECT * FROM tbl_standee";
$result = $connection->query($select);
$standees = array ();
while ($standee = $result->fetch_assoc()) :
	$standees[] = $standee;
endwhile;

?>

<div>
	<p style="color: blue">Please enter information to search</p>
	<table>
		<tr>
			<td style="width: 300px;">
				<p>
					<label for="txt-document-receive-reference-number">Reference Number</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-reference-number" class="input input-data new_theme_textbox ui-corner-all focus" name="reference_no" dir="15" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-origin">Document Origin</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-origin" class="input input-data new_theme_textbox ui-corner-all focus" name="origin_id" dir="15" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-name">Document Name</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-name" class="input input-data new_theme_textbox ui-corner-all focus" name="name" dir="15" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-category">Document Category</label>&nbsp;<span style="color: red;">*</span><br />
					<select id="txt-document-receive-category" class="input input-data new_theme_textbox ui-corner-all focus" name="category_id">
						<option value="">Select Document Category</option>
						<? foreach($document_categories as $category) : ?>
						<option value="<?= $category["id"] ?>"><?= $category["name"] ?></option>
						<? endforeach ?>
					</select>
				</p>
			</td>
			<td style="width: 300px;">
				<p>
					<label for="txt-document-receive-date-issue">Date of Document Issue</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-date-issue" class="calendar input input-data new_theme_textbox ui-corner-all focus" name="date_issue_doc" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-authority">Document Authority</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-authority" class="input input-data new_theme_textbox ui-corner-all focus" name="authority_id" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-date-receive">Date of Receive Document</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-date-receive" class="calendar input input-data new_theme_textbox ui-corner-all focus" name="date_receive_doc" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-folder-number">Folder Number</label>&nbsp;<span style="color: red;">*</span><br />
					<input id="txt-document-receive-folder-number" class="input input-data new_theme_textbox ui-corner-all focus" name="folder_no"type="text" />
				</p>
			</td>
			<td style="width: 300px;">
				<p>
					<label for="txt-document-receive-standee-number">Standee Number</label>&nbsp;<span style="color: red;">*</span><br />
					<select id="txt-document-receive-standee-number" class="input input-data new_theme_textbox ui-corner-all focus" name="dresn">
						<option value="">Select Standee Number</option>
						<? foreach($standees as $standee) : ?>
						<option value="<?= $standee["id"] ?>"><?= $standee["number"] ?></option>
						<? endforeach ?>
					</select>
				</p>
				<p>
					<label for="txt-document-receive-line-number">Standee Line Number</label>&nbsp;<span style="color: red;">*</span><br />
					<select id="txt-document-receive-line-number" class="input input-data new_theme_textbox ui-corner-all focus" name="line_no">
						<option value="">Select Standee</option>
					</select>
				</p>
				<p>
					<label for="txt-document-receive-source">Source</label><br />
					<input id="txt-document-receive-source" class="input input-data new_theme_textbox ui-corner-all focus" name="source" type="text" />
				</p>
				<p>
					<label for="txt-document-receive-other">Other</label><br />
					<input id="txt-document-receive-other" class="input input-data new_theme_textbox ui-corner-all focus" name="other" type="text" />
				</p>
			</td>
		</tr>
	</table>
	<br /><br />
	<span>Fields has asterisk(<span style="color: red;">*</span>)mark are required</span>
</div>

<script type="text/javascript">

	$(function() {
	
		var current_search_element;

		/* calendar ==================================================================*/

        $(".calendar").datepicker({
            dateFormat: "yy-mm-dd",
            changeMonth: true, changeYear: true, gotoCurrent: true
        });
        $(".ui-datepicker").css("font-size", "12px");
		$("#ui-datepicker-div").css("z-index", 2000);

		/* end calendar ==============================================================*/

		$("#txt-document-receive-number").focus().keydown(function(e) {
			if (e.keyCode == "13") {
				$(":button:contains(Save)").trigger("click");
			}
		});

		$("#txt-document-receive-standee-number").change(function() {
			var standee = $(this).val();
			$("#txt-document-receive-line-number").load("content/document/receive/get_standee_line.php", {
				"standee": standee
			});
		});
		
		$(".input-data").keyup(function(e) {		
			// var dialog_offset = $("#dlg-document-search").offset();
			var offset = $(this).offset();
			var auto_top = offset.top + 25;
			var auto_left = offset.left;
			current_search_element = $(this);
			
			$("#dlg-document-search-autocomplete")
			.css({						
				top: auto_top + "px",
				left: auto_left + "px",
				display: "block"
			});
			
			$.ajax({
				url: "content/search-document/search/search-autocomplete.php",
				type: "post",
				data: {
					field: $(this).attr("name"),
					text: $(this).val()
				},
				dataType: "html",
				success: function(result) {
					$("#dlg-document-search-autocomplete").html(result);
				}
			});
		});

		 $("#dlg-document-search").click(function() {
			$("#dlg-document-search-autocomplete").css("display", "none");
		 });
		 
		 $(".p-autocomplete").live("click", function() {
			current_search_element.val($(this).text());
			$("#dlg-document-search-autocomplete").css("display", "none");
		 });
	});

	function search_document() {
		var inputed_data = [];
		var empty = true;
		var data_index = 0;
		$(".input-data").each(function(i) {
			var index_name = $(this).attr("name");
			if ($(this).val() != "") {
				empty = false;
				inputed_data[data_index] = index_name + "#bv#" + $(this).val() + "#bv#" + $(this).attr("name");
				data_index++;
			}
		});

		if (empty == true) {
			$("<div>" +
				"<p style='color: red'>Warning: you cannot search the documents</p>" +
				"<blockquote>" +
				"<p>at least an information have to enter! or <b>Cancel</b> searchig</p>" +
				"</blockquote>" +
				"<br />" +
				"<p>Please try to search the document again.</p>" +
			"</div>")
			.dialog({
				title: "Message",
				width: 525, height: 247,
				modal: true, resizable: true,
				close: function() {
					$(this).dialog("destroy").remove();
				},
				buttons: {
					Ok: function() {
						$(this).dialog("close");
					}
				}
			});
			return false;
		}

		$.ajax({
			url: "content/search-document/search/search.php",
			type: "post", dataType: "html",
			data: { data: inputed_data },
			success: function(result) {
				$("#tbl-document-search tbody").html(result);
			}
		});
	}

</script>