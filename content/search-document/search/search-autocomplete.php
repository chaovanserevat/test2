<?php

include ("../../../connection.php");

$where = "";

if (isset ($_POST["field"])) {
	$field = $_POST["field"];
	if ($field == "reference_no") {
		$table = "tbl_document";
		$field_text = "reference_no";
	}
	elseif ($field == "name") {
		$table = "tbl_document";
		$field_text = "name";
	}
	elseif ($field == "source") {
		$table = "tbl_document";
		$field_text = "source";
	}
	elseif ($field == "other") {
		$table = "tbl_document";
		$field_text = "other";
	}
	elseif ($field == "origin_id") {
		$table = "tbl_document_origin";
		$field_text = "name";
	}
	elseif ($field == "authority_id") {
		$table = "tbl_document_authority";
		$field_text = "name";
	}
	elseif ($field == "folder_no") {
		$table = "tbl_folder";
		$field_text = "number";
	}
	
	if (isset ($_POST["text"])) {
			if ($_POST["text"] != "") {
			$text = $_POST["text"];
			$select_text = "SELECT $field_text From $table WHERE $field_text like '%$text%'";
		}
		else die("error code: s0003");
	}
	else die("error code: s0002");
}
else die("error code: s0001");

$get_text = $connection->query($select_text);
$text_list = array ();
while ($result = $get_text->fetch_row()) :
	$new_text = str_ireplace("$text", "<span style='color: blue'>$text</span>", $result[0]);
	$text_list[] = $new_text;
endwhile;

?>

<style type="text/css">

	.p-autocomplete:hover {
		cursor: pointer;
		background-color: "#E8E8E7";
	}

</style>

<? if (count($text_list) > 0) : ?>
<? $i = 1 ?>
    <?php foreach ($text_list as $autocomplete_text) : ?>
	<p class="p-autocomplete"><?= $autocomplete_text ?></p>
    <?php endforeach; ?>
<? else : ?>
	<p style="color: red">Empty!</p>
<? endif ?>