<style type="text/css">

.link-edit img, .link-delete img {
	width: 16px;
	height: 16px;
}

#tbl-document-search tbody tr:nth-child(odd) { background: #EDF7F8 }
#tbl-document-search tbody tr:nth-child(even) { background: #F8F9ED }
#tbl-document-search tbody > tr:hover { background: #FFF }

</style>

<div>
	<p>
		<button id="btn-document-advance-search">Advance Search</button>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<label for="from-date">From Date: &nbsp;</label><input id="txt-from-date" class="calendar new_theme_textbox ui-corner-all" style="width: 100px;" type="text" />&nbsp;&nbsp;
		<label for="to-date">To Date: &nbsp;</label><input id="txt-to-date" class="calendar new_theme_textbox ui-corner-all" style="width: 100px;" type="text" />&nbsp;&nbsp;
		<button id="btn-document-search">Search Document</button>
	</p>
	
	<center>
		<div style="overflow-x: scroll;">
			<p id="table-status" style="display: none; padding: 10px;"></p>

			<table id="tbl-document-search" class="nk-tbl-data" style="width: 100%;">
				<colgroup>
					<col style="width: auto;" />
					<col style="width: 200px;" />
					<col style="width: 200px;" />
					<col style="width: 200px;" />
					<col style="width: 200px;" />
					<col style="width: 200px;" />
					<col style="width: 200px;" />
					<col style="width: 200px;" />
					<col style="width: 30px;" />
					<col style="width: 30px;" />
				</colgroup>
				<thead>
					<tr class="ui-widget-header">
						<th><input type="checkbox" /></th>
						<th>Reference Number</th>
						<th>Document Origin</th>
						<th>Document Name</th>
						<th>Category</th>
						<th>Date of Issue</th>
						<th>Document Authority</th>
						<th>Date of Receive</th>
						<th colspan="2"></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="align-center ui-state-error" colspan="4">Data not found!</td>
					</tr>	
				</tbody>
			</table>
		</div>
	</center>	
	<div id="dialog-container-1" class="dialog-container"></div>
<div>

<script type="text/javascript">

/* calendar ==================================================================*/

$(".calendar").datepicker({
	dateFormat: "yy-mm-dd",
	changeMonth: true, changeYear: true, gotoCurrent: true
});

$(".ui-datepicker").css("font-size", "12px");
$("#ui-datepicker-div").css("z-index", 2000);

/* end calendar ==============================================================*/

$("button").button();

$("#tbl-document-search tbody").load("content/search-document/search/reload.php", function() { $.fn.tickCheckbox() });

    /* create new document *********************************************/

    $("#btn-document-advance-search").click(function() {
        $("<div></div>", {
            id: "dlg-document-search",
            title: "Search Document"
        })
        .dialog({
            width: 890, height: 470,
            modal: true, resizable: false,
			draggable: false,
            close: function() {
                $(this).dialog("destroy").remove();
            },
            buttons: {
                Search: function() {
                    search_document();
                    $("#dlg-document-search").dialog("close");
                },
                Close: function() {
                    $("#dlg-document-search").dialog("close");
                }
            }
        })
        .html(
			$("<center></center>").html(
				$.ajax_loading().css({ "padding-top": 5 })
			)
		)
        .load("content/search-document/search/search-option.php");
    });

    /* ******************************************************************** */
		
	/* document edit ****************************************************** */
		
        $("#tbl-document-search").delegate(".link-edit", "click", function() {
            var document_id = $(this).attr("href").split("#");
            $("<div></div>", {
                id: "dlg-document-receive-edit",
                title: "Edit Document"
            })
            .dialog({
                modal: true, //resizable: false,
				width: 890, height: 470,
                close: function() {
                    $(this).dialog("destroy").remove();
                },
                buttons: {
                    Save: function() {
                        save_edit_document();
						$(this).dialog("close");
                    },
                    Close: function() {
                        $(this).dialog("close");
                    }
                }
            })
            .html(
                $("<center></center>").html(
                    $.ajax_loading().css({ "padding-top": 5 })
                )
            )
            .load("content/search-document/search/edit.php", {
                id: document_id[1]
            });
            return false;
        });
	
	/* ******************************************************************** */

	
	/* delete document **************************************************** */
		
	$("#tbl-document-search").delegate(".link-delete", "click", function() {
		var document_id = $(this).attr("href").split("#");
		$("<div></div>", {
			id: "dlg-document-receive-delete",
			title: "Delete Document"
		})
		.html("Are you sure you want to delete this document?")
		.dialog({
			modal: true, resizable: false,
			width: 300, height: 160,
			close: function() {
				$(this).dialog("destroy").remove();
			},
			buttons: {
				Yes: function() {
					$.ajax({
						url: "content/search-document/search/delete.php",
						type: "post", dataType: "html",
						data: { id: document_id[1] },
						success: function(num) {
							$("#tbl-document-search tbody").load("content/search-document/search/reload.php", function() {
								if (num > 0) {
									$("#table-status")
										.removeClass()
										.addClass("ui-state-error")
										.html("A document has been deleted successfully!")
										.stop(true, true).fadeTo(0, 1).hide()
										.slideDown("normal").delay(2000).fadeOut("slow");
								}
								
								$.fn.tickCheckbox();
							});
							
							$("#dlg-document-receive-delete").dialog("close");
						}
					});
				},
				No: function() {
					$(this).dialog("close");
				}
			}
		});
		return false;
	});
	
	$("#btn-document-search").click(function() {
		// $("#tbl-document-search tbody").load("content/search-document/search/search.php", {
			// from_date: $("#txt-from-date").val(),
			// to_date: $("#txt-to-date").val()
		// });
		
		$.ajax({
			url: "content/search-document/search/search.php",
			type: "post", dataType: "html",
			data: {
				from_date: $("#txt-from-date").val(),
				to_date: $("#txt-to-date").val()
			},
			success: function(result) {
				$("#tbl-document-search tbody").html(result);
			}
		});
	});
				
		$("<div id='dlg-document-search-autocomplete'></div>")
		.css({
			display: "none",
			position: "absolute",
			background: "none repeat scroll 0 0 #EEF1F1",
			border: "1px solid gray",
			padding: "10px",
			"z-index": "100000",
			"min-width": "260px"
		})
		.addClass("ui-corner-all")
		.appendTo("body");

</script>